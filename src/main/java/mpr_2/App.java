package mpr_2;

import mpr_2.db.AddressManager;
import mpr_2.db.PermissionManager;
import mpr_2.db.PersonManager;
import mpr_2.db.RoleManager;
import mpr_2.db.UserManager;
import mpr_2.domain.Address;
import mpr_2.domain.Permission;
import mpr_2.domain.Person;
import mpr_2.domain.Role;
import mpr_2.domain.User;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        Person person = new Person();
        person.setName("Jan Nowak");
        person.setYob(1990);
        PersonManager personManager = new PersonManager();
        personManager.addPerson(person);
        System.out.println(person);

        Address address = new Address();
        address.setCity("Gdansk");
        address.setCountry("Polska");
        address.setStreet("Kubusia Puchatka");
        address.setPostalCode("80-461");
        address.setHouseNumber("32");
        address.setLocalNumber("14");
        AddressManager addressManager = new AddressManager();
        addressManager.addAddress(address);

        Permission permission = new Permission();
        permission.setName("Full");
        PermissionManager permissionManager = new PermissionManager();
        permissionManager.addPermission(permission);

        User user = new User();
        user.setUsername("Nabuchodonozor");
        user.setPassword("canis");
        UserManager userManager = new UserManager();
        userManager.addUser(user);

        Role role = new Role();
        role.setName("Ważna");
        RoleManager roleManager = new RoleManager();
        roleManager.addRole(role);
}




    }
